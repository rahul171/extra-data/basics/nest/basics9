import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const Param1 = createParamDecorator(
  (data: string, context: ExecutionContext) => {
    console.log('----Param1 => createParamDecorator(())----');
    console.log('param =>', data);

    const request = context.switchToHttp().getRequest();
    const tmp = request.url;

    console.log('----END----');
    return tmp;
  }
);
