import {
  applyDecorators,
  createParamDecorator,
  ExecutionContext,
  Get,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';

export const Decorator2 = createParamDecorator(
  (data: any, context: ExecutionContext) => {
    console.log('Decorator2');
  },
);

export const Decorator3 = createParamDecorator(
  (data: any, context: ExecutionContext) => {
    console.log('Decorator3');
  },
);

export const Decorator4 = (...args) => {
  return applyDecorators(Get('/route1'), HttpCode(402));
};
