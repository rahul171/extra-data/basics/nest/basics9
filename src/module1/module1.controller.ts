import { Controller, Get, UseFilters, UseGuards } from '@nestjs/common';
import { Module1Service } from './module1.service';
import { Param1 } from '../common/decorators/param1.decorator';
import {
  Decorator2,
  Decorator4,
} from '../common/decorators/decorator2.decorator';

@Controller('module1')
export class Module1Controller {
  constructor(private module1Service: Module1Service) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Decorator4()
  route1(@Param1('abcd') data): string {
    console.log('@Param1 return data =>', data);
    return 'route1';
  }
}
